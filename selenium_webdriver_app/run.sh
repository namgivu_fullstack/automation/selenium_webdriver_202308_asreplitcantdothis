SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
PH=`cd $SH/.. && pwd`

cd $PH
    python3 -m pipenv run \
        python "$SH/_.py"
