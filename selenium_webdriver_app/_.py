"""
ref Bard chatbot [view this chat](https://g.co/bard/share/d68800eb6696),
    and as Bard can export to replit, I exported it to replit [here](https://replit.com/@NamG1/selenium-webdriver#)

pip package
python3 -m pipenv install  selenium webdriver_manager
"""
import os.path
#
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options


ChromeDriverManager().install()  # auto install any required chrome webdriver

'''option soas to run chrome webdriver properly/noerror'''
o = Options()
o.add_argument('--headless')  #NOTE we MUST use --headless mode then .set_window_size() to capture whole page height
o.add_argument("--disable-gpu")  #NOTE this is required wh/ run in container  ref. https://dev.to/googlecloud/using-headless-chrome-with-cloud-run-3fdp
o.add_argument("--no-sandbox")   #NOTE this is required wh/ run in container  ref. https://dev.to/googlecloud/using-headless-chrome-with-cloud-run-3fdp
wd = webdriver.Chrome(options=o)

# wd.get('https://www.google.com')                            # this has simple content
wd.get('https://www.google.com/search?q=selenium+webdriver')  # this has loong-height content
import time; time.sleep(2)  # give enough time for content loading to complete  #TODO use :waitfor to wait for pagelodingdone

THIS_FILE_D = os.path.dirname(__file__)

#region take snapshot
import time; time.sleep(2)  #TODO use :waitfor to wait for pagelodingdone

# set full width+height to capture the whole-height content
height = wd.execute_script('return document.documentElement.scrollHeight')
width  = wd.execute_script('return document.documentElement.scrollWidth')
wd.set_window_size(width=width + 100, height=height + 100)  # the trick here  # use +100 for buffer spaces

r = wd.save_screenshot(f'{THIS_FILE_D}/output.d/yymmdd.png')
#                     (must use FULL PATH here)
if not r: raise Exception()  # for full codeflow, check above .png saving results True/succeeded
#endregion take snapshot


#region save html source
with open(f'{THIS_FILE_D}/output.d/pagesource.html', 'w') as f: f.write(wd.page_source)  # this only save html file ref. ref https://g.co/bard/share/d18c686bbc35

effort00_pyautogui='''
import pyautogui
# open 'Save as...' to save html and assets  ref. https://stackoverflow.com/a/53966809/248616/home/namgivu/code/l/namgivu_fullstack/automation_eslenium/selenium_ewbdriver_202308_asreplitcantdothis/selenium_webdriver_app/output.d/pagesource.html
pyautogui.hotkey('ctrl', 's')
# pyautogui.getWindowsWithTitle('All Files')[0].show()  #NOTE this only works on Windows NOT Linux huhu
pyautogui.hotkey('ctrl', 'a')
pyautogui.hotkey('delete')
pyautogui.typewrite(f'{THIS_FILE_D}/output.d/pagesource.html')
pyautogui.hotkey('enter')
# import time; time.sleep(3)
'''

effort01_ActionChains_send_keys='''
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

import time; time.sleep(2)
ActionChains(wd).send_keys(Keys.CONTROL, 's').perform()  #NOTE this not working ie no 'Save File As' window popup appear  ref. https://stackoverflow.com/a/55765879/248616
'''
#endregion save html source

wd.close()
